
<?php get_header(); ?>
<div id="news">
    <div id="wrapper">
        <div class="l-mvBlock">
            <h1><img src="/images/news/img_mv_pc.svg" alt="新着情報　～TOPICS&NEWS～"><br>新着情報</h1>
        </div>
    
        <div class="l-messageBlock">
            <div class="l-inner996">
                <div class="l-messageBlock01 p-ver2 h2only">
                
                    <div class="l-messageBlock01-01">
                        <div class="l-messageBlock_white">
                            <h2>
                                <p class="p-title Mincho">
                                    石坂ファームからのお知らせ
                                </p>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-partsBlock01">
            <div class="l-inner">
                <div class="l-partsBlock01-table l-column2">
                    <?php if (have_posts()) : ?><?php while (have_posts() ) : the_post(); ?><div>
                    	<a href="<?php the_permalink(); ?>">
                            <p class="p-image p-newslist"><?php the_post_thumbnail('full'); ?></p>
                            <div class="p-message">
                                <h3><p class="p-title"><span><?php the_time('Y/m/d'); ?></span><?php the_title(); ?></p></h3>
                                <div class="p-message01 p-left Mincho p-newsdetailimage">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </a>
                    </div><?php endwhile; ?><?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                
                <div class="activity-nav">
                <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
                    $paginate_format = '';
                    $paginate_base = add_query_arg('paged', '%#%');
                    } else {
                    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
                    user_trailingslashit('page/%#%/', 'paged');;
                    $paginate_base .= '%_%';
                    }
                    echo paginate_links( array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $wp_query->max_num_pages,
                    //'mid_size' => 3,
                    'end_size'    => 0,
                    'mid_size'    => 1,
                    'prev_next'    => false,						
                    'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <?php wp_reset_postdata(); ?>
                
                <!--<div class="activity-nav">
                    <a class="page-numbers" href="">1</a>
                    ..
                    <span class="page-numbers current">5</span>
                    ..
                    <a class="page-numbers" href="">8</a>
                </div>-->
                </div>
        </div>
    </div>    
</div>

<?php get_footer(); ?>
