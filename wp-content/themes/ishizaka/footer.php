<div id="page-top">
    <div><a href="#"><img src="/images/common/btn_scroll_pc.png" alt="Page Top"></a></div>
</div>

<footer>
	<div class="l-footer">
        <div class="l-inner">
            <div class="l-footer01 Mincho">
                <div class="p-column01">
                    <p><img src="/images/common/img_footer_logo_pc.png" alt="石坂ファーム ISHIZAKAFARM"></p>
                </div>
                <div class="p-column02">
                    <p class="p-name">石坂ファーム</p>
                    <p class="p-message">
                        〒354-0045<br>
                        埼玉県入間群三芳町上富1589-2<br>
                        TEL：049-293-2335<br>
                        FAX：049-259-7636
                    </p>
                    <p class="p-button">
                        <a href="<?php echo home_url();?>/contact/">お問い合わせ</a>
                    </p>
                </div>
                <div class="p-column03">
                    <p class="p-main"><a href="/">HOME</a></p>
                    <p class="p-main"><a href="/farm/">農園のこと</a></p>
                    <p class="p-sub"><a href="/farm/value.html">大切にしていること</a></p>
                    <p class="p-sub"><a href="/farm/staff.html">スタッフ</a></p>
                    <p class="p-main"><a href="/vegetable/">野菜のこと</a></p>
                    <p class="p-sub"><a href="/vegetable/work.html">野菜づくり</a></p>
                    <p class="p-sub"><a href="/vegetable/lineup.html">収穫カレンダー</a></p>
                </div>
                <div class="p-column04">
                    <p class="p-main"><a href="/vision/">ミライの農業</a></p>
                    <p class="p-main"><a href="/academy/">畑とつながる体験</a></p>
                    <p class="p-main"><a href="/marche/">商品</a></p>
                    <p class="p-main"><a href="/request/">契約販売・受託栽培</a></p>
                    <p class="p-main"><a href="/kids/">キッズファーム</a></p>
                </div>
                <div class="p-column05">
                    <p class="p-main"><a href="/voice/">お客様の声</a></p>
                    <p class="p-main"><a href="<?php echo home_url();?>/news/">新着情報</a></p>
                    <p class="p-main"><a href="/access/">農園概要・アクセス</a></p>
                </div>
            </div>
            <div class="l-footer01-02">
            	<div>
                    <a href="https://ishizaka-group.co.jp/" target="_blank">石坂産業株式会社</a>｜
                </div><div>
                    <a href="https://ishizaka-group.co.jp/privacy/" target="_blank">プライバシーポリシー</a>
                </div>
            </div>
        </div>
        <div class="l-footer02 Mincho di_b_from768">
        	<p><img src="/images/common/img_footer_logo_pc.jpg" alt="石坂ファームロゴ"></p>
            <p>石坂ファーム</p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
