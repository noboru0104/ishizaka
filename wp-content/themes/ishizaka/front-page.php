
<?php get_header(); ?>

<div id="wrapper">
    
    <div class="l-sliderBlock">
    	<div class="l-sliderBlock01">
            <ul class="bxslider">
                <li><img class="p-backImage" src="/images/top/img_mv1_pc_test.png" alt=""></li>
                <li><img class="p-backImage" src="/images/top/img_mv1_pc_test.png" alt=""></li>
                <li><img class="p-backImage" src="/images/top/img_mv1_pc_test.png" alt=""></li>
                <li><img class="p-backImage" src="/images/top/img_mv1_pc_test.png" alt=""></li>
                <li><img class="p-backImage" src="/images/top/img_mv1_pc_test.png" alt=""></li>
            </ul>
            <div class="l-inner">
            	<div class="l-sliderBlock01-01">
                    <div class="l-sliderBlock_title">
                        <p>
                            <img src="/images/top/img_message01_pc.svg" alt="Farm To Table">
                        </p>
                    </div>
                    <div class="l-sliderBlock_white">
                        <h1>
                        	<p class="p-title">
                                私たちは環境から<br class="di_b_from480">農業を考える<br>グローバル基準の<br class="di_b_from480">オーガニックファームです。
                            </p>
                        </h1>
                        <p class="p-message">
                            私たち石坂ファームは、環境教育や<br class="di_b_from768">里山保全にも取り組む企業として、<br>「環境から見た農業」をテーマに、<br>従来の“野菜をつくる・売る”という<br class="di_b_from768">農業のあり方を超え、<br class="di_n_from768">社会における<br class="di_b_from768">第一次産業の新しい価値づくりを<br class="di_b_from768">目指し取り組んでいます。<br>限りある環境を守り、持続可能な<br class="di_b_from768">社会のためにあるべき農業のスタイルを<br class="di_b_from480">追求します。<br>グローバルなオーガニックファーム<br class="di_b_from768">として、世界レベルの基準を証明する<br class="di_b_from480">国際規格を取得。<br>地域に根ざした固有種野菜栽培や<br class="di_b_from768">伝統農法である落ち葉堆肥農法に<br class="di_b_from480">こだわり、<br class="di_n_from480">マーケットのオーナー、<br class="di_b_from768">レストランのシェフの皆様を刺激する<br>オリジナルな野菜をご提供していきます。
                        </p>
                        <p class="p-english">
                        	Ishizaka Farm is pursuing the perspectives of the agricultural needs in the environment and society for a sustainable society based on the theme of agriculture viewed from the environment.<br>International standards have been acquired to prove our global standard.  Our organic farm focuses on farming heirloom vegetables and traditional farming methods.
 						</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="l-aboutBlock">
        <div class="l-inner1000">
            <div class="l-aboutBlock-table">
                <h2><img src="/images/top/img_about01_1_pc.svg" alt="VALUE"><br><span>大切にしていること</span></h2>
                <p class="p-message">
                    本物の安心安全でおいしい野菜をお届けするために、<br class="di_n_from480">森と畑がつながり、生態系を<br class="di_b_from480">活かした農業にこだわり続ける<br class="di_b_from480">石坂ファームが、<br class="di_n_from480">大切にしている3<br class="di_b_from480">つのこだわりをご紹介します。
                </p>
                <div class="p-list">
                    <p>
                        <a class="is-pagescroll" href="/farm/value.html#01">グローバル基準の農業</a>
                    </p><br class="di_b_from480"><p>
                        <a class="is-pagescroll" href="/farm/value.html#02">オーガニックの固有種を栽培</a>
                    </p><br class="di_b_from480"><p>
                        <a class="is-pagescroll" href="/farm/value.html#03">落ち葉堆肥農法を継承</a>
                    </p>
                </div>
                <p class="p-aboutButton">
                	<a href="/request/"><span>契約販売・受託栽培について</span></a>
                </p>      
            </div>
        </div>
            
        <div class="l-inner">
            <div class="l-aboutBlock-table2">
                <div>
                    <a href="/marche/">
                        <p class="p-image"><img src="/images/top/img_about02_1_pc.png" alt="商品"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">MARCHE<br><span>商品</span></p></h3>
                            <p class="p-message01">石坂ファームでは、キッチンカーを活用した出張店舗でオーガニック野菜の販売を実施。話題の「ビーガン」料理も提供可能です。</p>
                        </div>
                    </a>
                </div><div>
                    <a href="/request/">
                        <p class="p-image"><img src="/images/top/img_about02_2_pc.png" alt="契約販売・受託栽培"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">REQUEST<br><span>契約販売・受託栽培</span></p></h3>
                            <p class="p-message01">料理、マーケット関係者の方々からのオーダーやご要望に合わせた野菜の販売・栽培も承っております。</p>
                        </div>
                    </a>
                </div><div>
                    <a href="/academy/">
                        <p class="p-image"><img src="/images/top/img_about02_3_pc.png" alt="畑とつながる体験"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">ACADEMY<br><span>畑とつながる体験</span></p></h3>
                            <p class="p-message01">環境保護の視点からシカの解体など、さまざまな体験をご用意。「キッチン・スタジオ」では、一流シェフによる食農育プログラムも開催。</p>
                        </div>
                    </a>
                </div><div>
                    <a href="/vegetable/work.html">
                        <p class="p-image"><img src="/images/top/img_about02_5_pc.png" alt="野菜づくり"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">WORK<br><span>野菜づくり</span></p></h3>
                            <p class="p-message01">落ち葉堆肥農法や露地野菜栽培にこだわり、手間ひまをかけた野菜づくりを実施。良い土をつくり、種から苗を育てています。</p>
                        </div>
                    </a>
                </div><div>
                    <a href="/kids/">
                        <p class="p-image"><img src="/images/top/img_about02_7_pc.png" alt="キッズファーム"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">KIDS FARM<br><span>キッズファーム</span></p></h3>
                            <p class="p-message01">｢環境｣や｢自然｣の大切さを伝えるため、デイキャンプや田植え、小麦の脱穀など、「食農育」を体験できるプログラムをご用意。</p>
                        </div>
                    </a>
                </div><div>
                    <a href="/vision/">
                        <p class="p-image"><img src="/images/top/img_about02_6_pc.png" alt="ミライの農業"></p>
                        
                        <div class="p-message">
                            <h3><p class="p-title">VISION<br><span>ミライの農業</span></p></h3>
                            <p class="p-message01">固有種野菜栽培のデータベース化、栄養価の可視化、三富野菜のブランド化へのチャレンジに取り組んでいます。</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="l-newsBlock">
        <div class="l-inner">
            <div class="l-newsBlock-table clear">
                <h2><p class="p-title"><img src="/images/top/img_new01_pc.svg" alt="Topics&News"><br><span>農園トピックス&amp;ニュース</span></p></h2>
                <?php
					//args
					$args = array(
						'post_type' => 'news',
						'posts_per_page' => '3',
						'order'=>'DESC',
						'orderby'=>'post_date',
					);
					// the query
					$the_query = new WP_Query($args);
				?>
                
                <?php if ($the_query->have_posts()) : ?>
				<?php while ($the_query-> have_posts() ) : $the_query->the_post(); ?>
                <div>
                    <a href="<?php the_permalink(); ?>">
                        <div class="p-list">
                            <p class="p-date Mincho"><?php echo get_the_time('Y.m.d'); ?></p>
                            <p class="p-message Mincho"><?php the_title(); ?></p>
                        </div>
                    </a><br>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
    <div class="l-snsBlock">
        <div class="l-inner">
            <div>
                <p class="p-title"><a href="https://twitter.com/ishizaka_farm/" target="_blank"><img src="/images/top/img_news02_1_pc.png" alt=""></a></p>
                <div class="p-detail">
                    <p>
                        <a class="twitter-timeline" href="https://twitter.com/ishizaka_farm?ref_src=twsrc%5Etfw" height="416">Tweets by ishizaka_farm</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </p>
                </div>
            </div><div>
                <p class="p-title"><a href="https://www.facebook.com/ishizakafarm/" target="_blank"><img src="/images/top/img_news02_2_pc.png" alt=""></a></p>
                <div class="p-detail">
                    <div class="fb-page" data-href="https://www.facebook.com/ishizakafarm/" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/ishizakafarm/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ishizakafarm/">石坂ファーム</a></blockquote></div>
                </div>
            </div><div>
                <p class="p-title"><a href="https://instawidget.net/v/user/ishizaka_farm/" target="_blank"><img src="/images/top/img_news02_3_pc.png" alt=""></a></p>
                <div class="p-detail p-insta">
                    <!-- InstaWidget -->
                    <a href="https://instawidget.net/v/user/ishizaka_farm" id="link-adb87a00bb9b693dd5bcdf95c263633bfab5438394078d044e0078a838e7daf7">@ishizaka_farm</a>
                    <script src="https://instawidget.net/js/instawidget.js?u=adb87a00bb9b693dd5bcdf95c263633bfab5438394078d044e0078a838e7daf7&width=200px"></script>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
