<?php

add_action('init', 'session_start1');
function session_start1(){
	session_start();
}

//カスタムヘッダー
add_theme_support(
'custom-header',
array(
	'width' =>1200,
	'height' =>300,
	'header_text' =>false,
	'default-image' => '%s/images/top/main_image.png',
	)
	);
	
	//カスタムメニュー
	register_nav_menus(
		array(
			'place_global' => 'グローバル',
			'place_utility' => 'ユーティリティ',
			)
		);
//headerにmagin-topが入った場合、これをなくすwp_head()
//add_filter( 'show_admin_bar', '__return_false' );

add_theme_support('post-thumbnails');

//wp_headのtitleタグを削除
remove_action('wp_head', '_wp_render_title_tag', 1);
		
//画像パスの置換（固定ページ用）
function imgpass_short($arg) {
	$content = str_replace('"images/', '"' . get_stylesheet_directory_uri() . '/images/',$arg);
	return $content;
}
add_action('the_content','imgpass_short');

//背景パスの置換（固定ページ用）
function imgbgpass_short($arg) {
	$content = str_replace('bgimages/', '' . get_stylesheet_directory_uri() . '/images/',$arg);
	return $content;
}
add_action('the_content','imgbgpass_short');

//pdfパスの置換（固定ページ用）
function pdfpass_short($arg) {
	$content = str_replace('pdf/', '' . get_stylesheet_directory_uri() . '/pdf/',$arg);
	return $content;
}
add_action('the_content','pdfpass_short');

//aリンクの置換（固定ページ用）
function linkpass_short($arg) {
	$content = str_replace('"link/', '"' . home_url() . '/',$arg);
	return $content;
}
add_action('the_content','linkpass_short');

//pタグ削除
add_action('init', function() {
	remove_filter('the_excerpt', 'wpautop');
	remove_filter('the_content', 'wpautop');
});
 
add_filter('tiny_mce_before_init', function($init) {
	$init['fontsize_formats'] = '10px 12px 14px 16px 18px 20px 24px 28px 32px 36px 42px 48px';
	$init['wpautop'] = false;
	$init['apply_source_formatting'] = false;
	return $init;
});

//PHPファイルをインクルード
function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/$file.php");
    return ob_get_clean();
} 
add_shortcode('myphp', 'Include_my_php');

function myPreGetPosts( $query ) {
	if ( is_admin() || ! $query->is_main_query() ){
		return;
 	}
	if ( $query->is_post_type_archive('news') ) {
		//ニュース一覧用
		$query->set('posts_per_page', 10);
		//$query->set('order', 'DESC');
//		$query->set('orderby', 'post_date');
		
		$query->set( 'tax_query', $taxquery );
		
	}
}
add_action('pre_get_posts','myPreGetPosts');



/**
 * my_error_message
 * @param string $error
 * @param string $key
 * @param string $rule（半角小文字）
 */

//function my_error_message( $error, $key, $rule ) {
//    if ( $key === 'contact_category' && $rule === 'noempty' ) {		
//		return 'ご応募内容は必須項目です。';
//    }
//	if ( $key === 'contact_naiyou' && $rule === 'noempty' ) {
//        return '合格科目・ご応募動機は必須項目です。';
//    }
//	if ( $key === 'contact_name' && $rule === 'noempty' ) {
//        return 'お名前は必須項目です。';
//    }
//	if ( $key === 'contact_email' && $rule === 'noempty' ) {
//        return 'メールアドレスは必須項目です。';
//    }
//	if ( $key === 'agree' && $rule === 'required' ) {
//		return '必ず「同意する」にチェックを入れてください';
//    }
//    return $error;
//}
//add_filter( 'mwform_error_message_mw-wp-form-34', 'my_error_message', 10, 3 );

//remove_filter('the_content', 'wpautop');
//remove_filter('the_excerpt', 'wpautop');

function ua_smt(){
	//ユーザーエージェントを取得
	$ua = $_SERVER['HTTP_USER_AGENT'];
	//スマホと判定する文字リスト
	$ua_list = array('iPhone','iPad','iPod','Android');
	foreach ($ua_list as $ua_smt) {
		//ユーザーエージェントに文字リストの単語を含む場合はTRUE、それ以外はFALSE
		if (strpos($ua, $ua_smt) !== false) {
			return true;
		}
	} return false;
}

// パンくずリスト
function breadcrumb(){
    global $post;
    $str ='';
    if(!is_home()&&!is_admin()){
        $str.= '<div id="breadcrumb" class="pankuzu"><div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
        $str.= '<a href="'. home_url() .'" itemprop="url"><span itemprop="title">TOP</span></a> &gt;</div>';
 
        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($ancestor) .'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor) .'</span></a> &gt;</div>';
                }
            }
        $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($cat -> term_id). '" itemprop="url"><span itemprop="title">'. $cat-> cat_name . '</span></a> &gt;</div>';
        } elseif(is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a></div>';
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_permalink($ancestor).'" itemprop="url"><span itemprop="title">'. get_the_title($ancestor) .'</span></a> &gt;</div>';
                }
            }
        } elseif(is_single() || is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. /*get_category_link($cat -> term_id)*/get_page_link( 123 ). '" itemprop="url"><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a> &gt;</div>';
        } else{
            $str.='<div>'. wp_title('', false) .'</div>';
        }
        $str.='</div>';
    }
    echo $str;


//バージョンアップ通知を非表示
add_filter('pre_site_transient_update_core', '__return_zero'); //WP本体
add_filter('site_option__site_transient_update_plugins', '__return_zero'); //プラグイン
remove_action ('wp_version_check','wp_version_check');
remove_action ('admin_init','_maybe_update_core');

//-------------------------------------------------------------------
//管理バーから更新を非表示
//-------------------------------------------------------------------
function remove_bar_menus( $wp_admin_bar ) {
    $wp_admin_bar->remove_menu( 'updates' ); // 更新
}
add_action('admin_bar_menu', 'remove_bar_menus', 201);

//-------------------------------------------------------------------
//管理画面のサブメニュー非表示
//-------------------------------------------------------------------
function remove_menus(){
	remove_submenu_page('index.php', 'update-core.php'); //ダッシュボード,更新
}
add_action('admin_menu', 'remove_menus');

//-------------------------------------------------------------------
//管理メニューのアップデートの数字を消す
//-------------------------------------------------------------------
function hide_admin_items() {
?>
<style type="text/css">
.update-plugins,update-count,#contextual-help-link-wrap,li#wp-admin-bar-updates {
    display:none !important;
}
</style>
<?php
}
add_action ('admin_head','hide_admin_items');

}

