<!doctype html>
<html lang="ja">
<head>
<meta name="robots" />

<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php bloginfo('name'); ?></title>

<?php if(is_post_type_archive('news')): ?>
<meta name="keywords" content="ニュース,オーガニック野菜,有機野菜,農園,農場,仕入れ,宅配">
<meta name="description" content="埼玉県の里山でオーガニック野菜や有機野菜をつくっている「石坂ファーム」からの新着情報です。都心から近い埼玉県の里山でオーガニック野菜や有機野菜をつくっている農園「石坂ファーム」。武蔵野の台地で古くから伝わる伝統農法で伝統野菜や固有種、ハーブなど、年間100種類ほどの野菜を栽培。「GLOBAL G.A.P.」「ASIA GAP」「有機JAS」など、国際・国内認証も取得し安全・安心な野菜づくりでおいしい野菜をお届けします。">
<link rel="canonical" href="https://www.ishizaka-farm.co.jp/news/" />
<meta property="og:title" content="農園トピックス&ニュース | オーガニック野菜・有機野菜・こだわり野菜の「石坂ファーム」" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.ishizaka-farm.co.jp/news/" />
<meta property="og:image" content="https://www.ishizaka-farm.co.jp/wp-content/uploads/2018/05/bg_mv01_pc.jpg" />
<meta property="og:site_name" content="オーガニック野菜・有機野菜・こだわり野菜の「石坂ファーム」" />
<meta property="og:description" content="埼玉県の里山でオーガニック野菜や有機野菜をつくっている「石坂ファーム」からの新着情報です。都心から近い埼玉県の里山でオーガニック野菜や有機野菜をつくっている農園「石坂ファーム」。武蔵野の台地で古くから伝わる伝統農法で伝統野菜や固有種、ハーブなど、年間100種類ほどの野菜を栽培。「GLOBAL G.A.P.」「ASIA GAP」「有機JAS」など、国際・国内認証も取得し安全・安心な野菜づくりでおいしい野菜をお届けします。" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="農園トピックス&ニュース | オーガニック野菜・有機野菜・こだわり野菜の「石坂ファーム」" />
<meta name="twitter:description" content="埼玉県の里山でオーガニック野菜や有機野菜をつくっている「石坂ファーム」からの新着情報です。都心から近い埼玉県の里山でオーガニック野菜や有機野菜をつくっている農園「石坂ファーム」。武蔵野の台地で古くから伝わる伝統農法で伝統野菜や固有種、ハーブなど、年間100種類ほどの野菜を栽培。「GLOBAL G.A.P.」「ASIA GAP」「有機JAS」など、国際・国内認証も取得し安全・安心な野菜づくりでおいしい野菜をお届けします。" />
<meta name="twitter:image" content="https://www.ishizaka-farm.co.jp/wp-content/uploads/2018/05/bg_mv01_pc.jpg" />
<meta itemprop="image" content="https://www.ishizaka-farm.co.jp/wp-content/uploads/2018/05/bg_mv01_pc.jpg" />
<?php endif; ?>

<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="/css/common/reset.css">
<link rel="stylesheet" type="text/css" href="/css/common/common.css">
<!-- ▲共通CSS▲ -->
<link rel="shortcut icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="stylesheet" type="text/css" href="/css/common/parts.css">
<!-- ▼個別CSS▼ -->
<?php if(is_front_page()): ?>
<link rel="stylesheet" type="text/css" href="/css/top.css">
<link rel="stylesheet" href="/css/common/animate.css">
<link rel="stylesheet" href="/css/common/jquery.bxslider.css">
<?php endif; ?>
<?php if(is_post_type_archive('news')||is_singular('news')): ?>
<link rel="stylesheet" type="text/css" href="/css/news/news.css">
<?php endif; ?>
<?php if(is_page('contact')||is_page('complete')): ?>
<link rel="stylesheet" type="text/css" href="/css/contact/contact.css">
<?php endif; ?>

<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/js/common/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/js/common/parts.js"></script>
<?php if(is_front_page()): ?>
<script type="text/javascript" src="/js/top.js"></script>
<script type="text/javascript" src="js/common/jquery.bxslider.min.js"></script>
<?php endif; ?>
<?php if(is_page('contact')): ?>
<script src="//ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/contact/contact.js"></script>
<?php endif; ?>
<!-- ▲個別JS▲ -->
<?php wp_head()?>
</head>
<?php if(is_page('contact')||is_page('complete')): ?>
<body id="contact">
<?php else: ?>
<body>
<?php endif; ?>
<?php if(is_front_page()): ?>
<!--facebookタイムライン表示に必要-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.0&appId=140875656694491';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php endif; ?>
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <div>
                    <h1><a href="/"><img src="/images/common/img_header_logo_pc.png" alt="石坂ファーム ISHIZAKAFARM"></a></h1>
                </div>
            </div>
            <div class="l-header-right">
            	<div class="l-header-right01 pconly">
                	<div>
                    	<a href="<?php echo home_url();?>/news/">新着情報</a>｜
                    </div><div>
                    	<a href="/access/">アクセス</a>｜
                    </div><div>
                    	<a href="/voice/">お客様の声</a>｜
                    </div><div>
                    	<a href="<?php echo home_url();?>/contact/">お問い合わせ</a>
                    </div>
                    <p><img src="/images/common/img_header_logo2_1_pc.png" alt="GGAP"></p>
                    <p><img src="/images/common/img_header_logo2_2_pc.png" alt="AGAP"></p>
                    <p><img src="/images/common/img_header_logo2_3_pc.png" alt="JGAP"></p>
                </div>
                <div class="l-header-right02 pconly">
                	<div>
                    	<a href="/farm/">FARM<span>農園のこと</span></a>
                    </div><div>
                    	<a href="/vegetable/">VEGETABLE<span>野菜のこと</span></a>
                    </div><div>
                    	<a href="/vision/">VISION<span>ミライの農業</span></a>
                    </div><div>
                    	<a href="/academy/">ACADEMY<span>畑とつながる体験</span></a>
                    </div><div>
                    	<a href="/marche/">MARCHE<span>商品</span></a>
                    </div><div>
                    	<a href="/request/">REQUEST<span>契約販売・受託栽培</span></a>
                    </div>
                </div>
                <!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly0">
                	<div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
              
        <nav>
            <div id="global-nav-area">
                <ul id="global-nav" class="Mincho">
                    <li class="title"><p><a href="/">HOME</a></p></li>
                    <li class="title">
                    	<p><a href="/farm/">農園のこと</a></p>
                        <div class="p-table">
                        	<div>
                            	<p class="p-left"><a href="/farm/value.html">大切にしていること</a></p>
                            </div><div>
                            	<p class="p-right"><a href="/farm/staff.html">スタッフ</a></p>
                            </div>
                        </div>
                    </li>
                    <li class="title">
                    	<p><a href="/vegetable/">野菜のこと</a></p>
                        <div class="p-table">
                        	<div>
                            	<p class="p-left"><a href="/vegetable/work.html">野菜づくり</a></p>
                            </div><div>
                            	<p class="p-right"><a href="/vegetable/lineup.html">収穫カレンダー</a></p>
                            </div>
                        </div>
                    </li>
                    <li class="title p-sub">
                    	<div class="p-table">
                        	<div>
                            	<p class="p-left p-category"><a href="/vision/">ミライの農業</a></p>
                            </div><div>
                            	<p class="p-right p-category"><a href="/academy/">畑とつながる体験</a></p>
                            </div><div>
                            	<p class="p-left p-category"><a href="/marche/">商品</a></p>
                            </div><div>
                            	<p class="p-right p-category"><a href="/request/">契約販売・受託栽培</a></p>
                            </div><div>
                            	<p class="p-left p-category"><a href="/kids/">キッズファーム</a></p>
                            </div><div>
                            	<p class="p-right p-category p-blank">&nbsp;</p>
                            </div>
                        </div>
                    </li>
                    <li class="title p-sub">
                    	<div class="p-table">
                        	<div>
                            	<p class="p-left p-category"><a href="/voice/">お客様の声</a></p>
                            </div><div>
                            	<p class="p-right p-category"><a href="<?php echo home_url();?>/news/">新着情報</a></p>
                            </div><div>
                            	<p class="p-left p-category"><a href="/access/">農園概要・アクセス</a></p>
                            </div><div>
                            	<p class="p-right p-category"><a href="<?php echo home_url();?>/contact/">お問い合わせ</a></p>
                            </div>
                        </div>
                    </li>                    
                    <li class="p-close"><p>× close</p></li>
                </ul>
            </div>
        </nav>
    </div>
</header>

