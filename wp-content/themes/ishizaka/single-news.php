<?php

get_header(); ?>
<?php
	if (have_posts()) :
	while ( have_posts() ) : the_post();
?>
<div id="news">
    <div id="wrapper">
        
        <div class="l-mvBlock">
            <h1><img src="/images/news/img_mv_pc.svg" alt="新着情報　～TOPICS&NEWS～"><br>新着情報</h1>
        </div>
        
        <div class="l-messageBlock">
            <div class="l-inner996">
                <div class="l-messageBlock01 p-ver2 h2only">
                
                    <div class="l-messageBlock01-01">
                        <div class="l-messageBlock_white">
                            <h2>
                                <p class="p-title Mincho">
                                    石坂ファームからのお知らせ
                                </p>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <div class="l-partsBlock03">    
            <div class="l-valueBlock01">
                <div class="l-inner">
                    <div class="p-title p-normal">
                        <h3><p class="Mincho"><span><?php the_time('Y/m/d'); ?></span><?php the_title(); ?></p></h3>
                    </div>
                    <div class="l-valueBlock02_table">
                        <div class="l-left">
                            <div class="l-valueBlock02_white">
                                <div class="p-message Mincho p-news">
                                    <p class="p-image p-newsdetail"><?php the_post_thumbnail('full'); ?></p>
									<?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        
        <div class="l-buttonBlock">
            <div class="l-inner">
                <div class="l-buttonBlock-table">
                    <div class="p-arrowButton">
                        <p>
                            <a class="Mincho" href="/news/"><span>新着情報一覧に戻る</span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>


<?php 
	endwhile;
	endif;
	
?>
<?php get_footer(); ?>