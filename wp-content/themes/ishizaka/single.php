<?php

get_header(); ?>

<div id="wrapper">
	
    <div class="l-mvBlock">
        <h1><img src="../images/news/img_mv_pc.svg" alt="新着情報　～TOPICS&NEWS～"><br>新着情報</h1>
    </div>
    
    <div class="l-messageBlock">
        <div class="l-inner996">
        	<div class="l-messageBlock01 p-ver2 h2only">
            
                <div class="l-messageBlock01-01">
                    <div class="l-messageBlock_white">
                        <h2>
                            <p class="p-title Mincho">
                                石坂ファームからのお知らせ
                            </p>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="l-partsBlock03">    
        <div class="l-valueBlock01">
            <div class="l-inner">
                <div class="p-title p-normal">
                    <h3><p class="Mincho"><span>2018/00/00</span>石坂ファームのWebサイトがオープンしました！</p></h3>
                </div>
                <div class="l-valueBlock02_table">
                    <div class="l-left">
                        <div class="l-valueBlock02_white">
                            <p class="p-message Mincho">
                                平素より格別のお引き立てを賜り、厚く御礼申し上げます。<br>
                                この度より多くのお客様に知っていただくべく、webサイトを新規オープンいたしました。<br>
                                今後はより一層の内容充実に努めてまいりますので、今後ともどうぞよろしくお願い申し上げます。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    
    <div class="l-buttonBlock">
        <div class="l-inner">
            <div class="l-buttonBlock-table">
            	<div class="p-arrowButton">
                    <p>
                        <a class="Mincho" href="../news/"><span>新着情報一覧に戻る</span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</div>

<?php get_footer(); ?>